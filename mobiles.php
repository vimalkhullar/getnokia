<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<script src='titlebar.js' type='text/javascript' ></script>
		<link rel='stylesheet' type='text/css' href='titlebar.css' />
		<link rel='stylesheet' type='text/css' href='http://cdn.webrupee.com/font' />

		<?php
			session_start();
			require('connect.php');
			$con = connect_db();
			if(!$_GET): header("location:error.php"); endif;
			extract($_GET);
			$title = mysql_query("SELECT phone_name FROM phones WHERE model = '$model'");
			$title = mysql_fetch_array($title);
			$title = $title[0];
			echo "<title>$title - Get Nokia</title>";

			function getValue($val)
			{
				global $model;
				$query="SELECT " . $val . " FROM phones where model='" . $model . "'";
				$res=mysql_query($query);
				while ($row = mysql_fetch_array($res))
				{
					if($val=='ram'):
						if($row[$val]>1000):
							return($row[$val]/1024 . " GB");
						else:
							return($row[$val] . " MB");
						endif;
					else:
						return($row[$val]);
					endif;
				}
			}
			$image='';
			function getImage($val)
			{
				global $model,$image;
				$image="'imgs/". $model . "/" . $model . $val . ".jpg'";
				echo "background-image:url($image);";
			}
		?>

		<style type='text/css'>
			table.mobile1{
				margin:auto;
				margin-top:50px;
				border:2px solid #0072C6;
				width:900px;
				height:400px;
				border-collapse:collapse;
			}

			table.mobile1 ul.one{
				list-style-type:none;
				text-align:left;
			}

			ul.one li{
				padding:0px 20px 0px 20px;
				display:inline-block;
			}

			table.mobile1 ul.two{
				list-style-image:url('imgs/tick.png');
				text-align:left;
			}

			ul.two li{
				padding:5px 10px 5px 10px;
			}

			.img{
				text-align:center;
				background-repeat:no-repeat;
				background-position:center;
				background-size:contain;
				height:300px;
				width:250px;
			}

			.img_small{
				height:60px;
				width:40px;
				text-align:center;
				border-bottom:2px solid #0072C6;
				background-repeat:no-repeat;
				background-position:center;
				background-size:contain;
			}

			.img_small:hover{
				border-color:red;
				opacity:0.5;
			}

			.header{
				background-color:#0072C6;
				font-size:xx-large;
				text-align:center;
			}

			table.mobile2, table.comments{
				margin:auto;
				margin-top:50px;
				border:2px solid #0072C6;
				width:900px;
				display:none;
			}

			table.mobile2 td{
				padding:10px 5px 10px 5px;
			}

			table.mobile2 td.head, table.comments td.head{
				background-color:#0072C6;
				font-size:x-large;
				text-align:center;
			}

			table.mobile2 td.head{
				padding:0px 80px 0px 80px;
			}

			td.lside{
				background-color:#0072C6;
				font-variant:small-caps;
				text-align:center;
				font-size:large;
				padding:0px 20px 0px 20px!important;
			}
		</style>

		<script type='text/javascript'>
			function imageChange(image)
			{
				document.getElementById('main_image').style.backgroundImage="url(" + image + ")";
			}
			function showSpecs()
			{
				document.getElementsByClassName('comments')[0].style.display='none';
				document.getElementsByClassName('comments')[1].style.display='none';
				document.getElementById('specs').style.display='table';
			}
			function showComments()
			{
				document.getElementById('specs').style.display='none';
				document.getElementsByClassName('comments')[0].style.display='table';
				document.getElementsByClassName('comments')[1].style.display='table';
			}
		</script>
	</head>
	<body>
		<div class='back'>
			<div class='front'>
				<ul>
					<li style='vertical-align:initial;'><a href='http://getnokia.tk/'><img src='imgs/main.png' style='border:none;outline:none;padding-left:20px;'/></a></li>
					<li style='padding:24px 20px 24px 2in!important;vertical-align:top;'>
						Search: <input type='text' class='textbox' onkeyup='show_sr_box(this)'
						onclick='show_sr_box(this)' onmousemove='show_sr_box(this)'/>
						<div class='sr_box' id='sr_box' onmousemove="this.style.display='block';" onmouseout="this.style.display='none';"></div>
					</li>
					<li class='hover' onclick="location.href='compare.php'">Phone Fight</li>
					<?php	if(isset($_SESSION['user'])):	?>
						<li class='hover' onmouseover='show_user_ctrl()' onmousemove='show_user_ctrl()' onmouseout='hide_user_ctrl()'>
							<?php
								$res=mysql_query("SELECT fname, lname FROM users WHERE username = '" . $_SESSION['user'] . "'");
								$row=mysql_fetch_assoc($res);
								echo $_SESSION['user'] . " (" . $row['fname'] . " " . $row['lname'] . ")";
							?>

							<div class='main_box' id='main_box'	onmousemove="this.style.display='block';" onmouseout="this.style.display='none';">
									<ul>
										<li onclick="location.href='myCart.php';">Cart</li>
										<li onclick="location.href='orders.php';">Orders</li>
										<li onclick="location.href='myinfo.php';">Settings</li>
										<li onclick="location.href='fileReturn.php';">File Return</li>
										<?php	if($_SESSION['user'] === "admin"):	?>
												<li onclick="location.href='update.php';">Update</li>
												<li onclick="location.href='reports.php';">View reports</li>
										<?php endif;	?>
										<li onclick="location.href='logout.php';">Log Out</li>
									</ul>
							</div>
						</li>
					<?php else:	?>
						<li class='hover' onclick="location.href='login.php'">Login</li>
						<li class='hover' onclick="location.href='register.php'">Register</li>
					<?php endif;	?>
				</ul>
			</div>
		</div>
		<div class='gap1'></div>
		<table class='mobile1'>
			<tr>
				<td colspan='3' class='header'><?php echo getValue('phone_name'); ?></td>
			</tr>
			<tr><td colspan='3' style='padding-top:5px;'/></tr>
			<tr>
				<td rowspan='5' class='img' style="<?php getImage(''); ?>" id='main_image'/>
				<td class='img_small' style="<?php getImage(''); ?>" onmouseover="imageChange(<?php echo $image;	?>)" /></td>
				<td>
					<ul class='one'>
						<li><span class='bold'>Price: <span class="WebRupee">Rs </span><?php echo getValue('price'); ?></span></li>
						<li><?php
								$val=getValue('qty_available');
								if($val == 0):
									echo "<li>Out of stock!!!</li>";
								else:
									echo "<form action='myCart.php' method='post'>";
									echo "<input type='text' name='model' value='$model' style='display:none;' />";
									echo "<li>Qty - <input type='number' min='1' max='$val' name='qty_ordered' value='1' /></li>";
									echo "<li><input type='submit' value='BUY NOW' name='order' /></li>";
									echo "</form>";
								endif;
							?>
						</li>
					</ul>
				</td>
			</tr>
			<tr>
				<td class='img_small' style="<?php getImage('-1'); ?>" onmouseover="imageChange(<?php echo $image;	?>)" />
				<td rowspan='2'>
					<ul class='two'>
						<li>Call <span class='bold'>Vimal Khullar</span> for assistance from our product expert.</li>
						<li><span class='bold'>Cash</span> on Delivery</li>
						<li><span class='bold'>30 Day</span> Replacement Guarantee</li>
						<li><span class='bold'>Free Shipping</span> to all over <span class='bold'>India</span>
					</ul>
				</td>
			</tr>
			<tr>
				<td class='img_small' style="<?php getImage('-2'); ?>" onmouseover="imageChange(<?php echo $image;	?>)" />
			</tr>
			<tr>
				<td class='img_small' style="<?php getImage('-3'); ?>" onmouseover="imageChange(<?php echo $image;	?>)" />
				<td style='font-size:small;text-align:center;'>1 year manufacturer warranty for Phone and 6 months warranty for in the box accessories <br/><span class='bold'>Nokia India</span> Warranty and Free Transit Insurance.</td>
			</tr>
			<tr>
				<td class='img_small' style="<?php getImage('-4'); ?>" onmouseover="imageChange(<?php echo $image;	?>)" />
				<td>
					<ul class='one' style='text-align:center;'>
						<li><input type='button' class='button' value='Specs' onclick='showSpecs()'/></li>
						<li><input type='button' class='buttom' value='Comments' onclick='showComments()'/></li>
					<ul>
				</td>
			</tr>
			<tr><td colspan='3' style='padding-top:5px;'/></tr>
		</table>
		<table class='mobile2' id='specs'>
			<tr>
				<td rowspan='2' class='head'>GENERAL</td>
				<td class='lside'>Status</td>
				<td class='rside' colspan='4'><?php echo getValue('status'); ?></td>
			</tr>

			<tr>
				<td class='lside'>SIM</td>
				<td class='rside' colspan='4'><?php echo getValue('sim'); ?></td>
			</tr>

			<tr>
				<td rowspan='2' class='head'>BODY</td>
				<td class='lside'>Dimensions</td>
				<td class='rside' colspan='4'>(<?php echo getValue('dim'); ?> in)</td>
			</tr>

			<tr>
				<td class='lside'>Weight</td>
				<td class='rside' colspan='4'><?php echo getValue('weight'); ?> g</td>
			</tr>

			<tr>
				<td rowspan='4' class='head'>DISPLAY</td>
				<td class='lside'>Screen</td>
				<td class='rside' colspan='4'><?php echo getValue('screen'); ?></td>
			</tr>

			<tr>
				<td class='lside'>Size</td>
				<td class='rside' colspan='4'><?php echo getValue('size'); ?> in</td>
			</tr>

			<tr>
				<td class='lside'>Resolution</td>
				<td class='rside' colspan='4'><?php echo getValue('res'); ?></td>
			</tr>

			<tr>
				<td class='lside'>Protection</td>
				<td class='rside' colspan='4'><?php echo getValue('protect'); ?></td>
			</tr>

			<tr>
				<td rowspan='3' class='head'>MEMORY</td>
				<td class='lside'>RAM</td>
				<td class='rside' colspan='4'><?php echo getValue('ram'); ?></td>
			</tr>

			<tr>
				<td class='lside'>Internal</td>
				<td class='rside' colspan='4'><?php echo getValue('internal'); ?> GB</td>
			</tr>

			<tr>
				<td class='lside'>Card Slot</td>
				<td class='rside' colspan='4'><?php echo getValue('card'); ?></td>
			</tr>

			<tr>
				<td rowspan='4' class='head'>DATA</td>
				<td class='lside'>WLAN</td>
				<td class='rside' colspan='4'><?php echo getValue('wlan'); ?></td>
			</tr>

			<tr>
				<td class='lside'>Bluetooth</td>
				<td class='rside' colspan='4'><?php echo getValue('bt'); ?></td>
			</tr>

			<tr>
				<td class='lside'>NFC</td>
				<td class='rside' colspan='4'><?php echo getValue('nfc'); ?></td>
			</tr>

			<tr>
				<td class='lside'>USB</td>
				<td class='rside' colspan='4'><?php echo getValue('usb'); ?></td>
			</tr>

			<tr>
				<td rowspan='4' class='head'>CAMERA</td>
				<td class='lside'>Primary</td>
				<td class='rside' colspan='4'><?php echo getValue('main'); ?></td>
			</tr>

			<tr>
				<td class='lside'>Additional</td>
				<td class='rside' colspan='4'><?php echo getValue('addl'); ?></td>
			</tr>

			<tr>
				<td class='lside'>Video</td>
				<td class='rside' colspan='4'><?php echo getValue('video'); ?></td>
			</tr>

			<tr>
				<td class='lside'>Secondary</td>
				<td class='rside' colspan='4'><?php echo getValue('secondary'); ?></td>
			</tr>

			<tr>
				<td rowspan='4' class='head'>FEATURES</td>
				<td class='lside'>OS</td>
				<td class='rside' colspan='4'><?php echo getValue('os'); ?></td>
			</tr>

			<tr>
				<td class='lside'>CPU</td>
				<td class='rside' colspan='4'><?php echo getValue('cpu'); ?></td>
			</tr>

			<tr>
				<td class='lside'>GPU</td>
				<td class='rside' colspan='4'><?php echo getValue('gpu'); ?></td>
			</tr>

			<tr>
				<td class='lside'>Chipset</td>
				<td class='rside' colspan='4'><?php echo getValue('chip'); ?></td>
			</tr>

			<tr>
				<td rowspan='3' class='head'>BATTERY</td>
				<td class='lside'>Power</td>
				<td class='rside' colspan='4'><?php echo getValue('power'); ?> mAh</td>
			</tr>

			<tr>
				<td class='lside'>Stand-by</td>
				<td class='rside' colspan='4'><?php echo getValue('stand'); ?></td>
			</tr>

			<tr>
				<td class='lside'>Talk-time</td>
				<td class='rside' colspan='4'><?php echo getValue('talk'); ?></td>
			</tr>

		</table>

		<table class='comments' style='border-collapse:collapse;'>
			<?php
				$query="SELECT username,comments FROM comments WHERE model='$model'";
				$res=mysql_query($query);
				if(mysql_affected_rows($con)):
					while ($row = mysql_fetch_array($res))
					{
						echo "<tr><td class='head'>" . $row['username'] . "</td></tr>";
						echo "<tr><td>" . $row['comments'] . "</td></tr>";
					}
				else:
					echo "<tr><td class='head'>No comments/reviews yet!!!</td></tr>";
				endif;
			?>
		</table>

		<form action='icomments.php' method='post'>
			<table class='comments' style='border-collapse:collapse;'>
				<tr>
					<td class='head'>Write your opinions/review -</td>
					<td class='head'>
						<input type='text' style='display:none;' name='model' value='<?php echo $model; ?>'/>
						<input type='submit' name='com' class='button' value='Submit'/>
					</td>
				</tr>
				<tr>
					<td colspan='2'>
						<textarea class='textbox' name='new_comment' style='width:900px;height:50px;border:none;resize:none;'></textarea>
					</td>
				</tr>
			</table>
		</form>
		<div class='gap2'></div>
		<div class='footer'>
			This site is a <span class='bold'>college project</span> made by <span class='bold'>Vimal Khullar</span>
		</div>
	</body>
</html>
