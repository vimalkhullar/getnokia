<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Get Nokia</title>
		<script src='titlebar.js' type='text/javascript' ></script>
		<script src='formValidation.js' type='text/javascript'></script>
		<link rel='stylesheet' type='text/css' href='titlebar.css' />
		<?php
			session_start();
			if(isset($_SESSION['user'])): header("location:http://getnokia.tk/"); endif;
			require('connect.php');
			$con = connect_db();
		?>
		<style type='text/css'>
			table.input{
				border:2px solid #0072C6;
				margin:auto;
				margin-top:50px;
				width:5in;
				border-collapse:collapse;
				text-align:center;
			}

			table.input th{
				background-color:#0072C6;
				color:white;
				padding:2px;
				font-size:105%;
			}

			table.input td{
				padding-top:10px;
			}
		</style>
	</head>
	<body>
		<div class='back'>
			<div class='front'>
				<ul>
					<li style='vertical-align:initial;'><a href='http://getnokia.tk/'><img src='imgs/main.png' style='border:none;outline:none;padding-left:20px;'/></a></li>
					<li style='padding:24px 20px 24px 2in!important;vertical-align:top;'>
						Search: <input type='text' class='textbox' onkeyup='show_sr_box(this)'
						onclick='show_sr_box(this)' onmousemove='show_sr_box(this)'/>
						<div class='sr_box' id='sr_box' onmousemove="this.style.display='block';" onmouseout="this.style.display='none';"></div>
					</li>
					<li class='hover' onclick="location.href='compare.php'">Phone Fight</li>
					<?php	if(isset($_SESSION['user'])):	?>
						<li class='hover' onmouseover='show_user_ctrl()' onmousemove='show_user_ctrl()' onmouseout='hide_user_ctrl()'>
							<?php
								$res=mysql_query("SELECT fname, lname FROM users WHERE username = '" . $_SESSION['user'] . "'");
								$row=mysql_fetch_assoc($res);
								echo $_SESSION['user'] . " (" . $row['fname'] . " " . $row['lname'] . ")";
							?>

							<div class='main_box' id='main_box'	onmousemove="this.style.display='block';" onmouseout="this.style.display='none';">
									<ul>
										<li onclick="location.href='myCart.php';">Cart</li>
										<li onclick="location.href='orders.php';">Orders</li>
										<li onclick="location.href='myinfo.php';">Settings</li>
										<li onclick="location.href='fileReturn.php';">File Return</li>
										<?php	if($_SESSION['user'] === "admin"):	?>
												<li onclick="location.href='update.php';">Update</li>
												<li onclick="location.href='reports.php';">View reports</li>
										<?php endif;	?>
										<li onclick="location.href='logout.php';">Log Out</li>
									</ul>
							</div>
						</li>
					<?php else:	?>
						<li class='hover' onclick="location.href='login.php'">Login</li>
						<li class='hover' onclick="location.href='register.php'">Register</li>
					<?php endif;	?>
				</ul>
			</div>
		</div>
		<div class='gap1'></div>
		<form action="" method="post">
			<table class='input'>
				<tr>
					<th colspan='2' style='text-align:center;font-weight:bold;'>All fields are mandatory!!!</th>
				</tr>
				<tr>
					<td class='padding'>Username:</td>
					<td><span class='err'></span><input type='text' name='username' class='textbox' id='t1' onclick='usernameCheck(this,0)' onkeyup='usernameCheck(this,0)' onblur='removeMark(0)'/></td>
				</tr>

				<tr>
					<td class='padding'>First Name:</td>
					<td>
						<span class='err'></span>
						<input type='text' name='fname' class='textbox' onclick='nameCheck(this,1)' onkeyup='nameCheck(this,1)' onblur='removeMark(1)'/>
					</td>
				</tr>

				<tr>
					<td class='padding'>Last Name:</td>
					<td>
						<span class='err'></span>
						<input type='text' name='lname' class='textbox' onclick='nameCheck(this,2)' onkeyup='nameCheck(this,2)' onblur='removeMark(2)'/>
					</td>
				</tr>

				<tr>
					<td class='padding'>Password:</td>
					<td>
						<span class='err'></span>
						<input type='password' name='pass1' class='textbox' id='pass' onclick='passCheck(this,3)' onkeyup='passCheck(this,3)' onblur='removeMark(3)'/>
					</td>
				</tr>

				<tr>
					<td class='padding'>Confirm Password:</td>
					<td>
						<span class='err'></span>
						<input type='password' name='pass2' class='textbox' onclick='passCheck(this,4)' onkeyup='passCheck(this,4)' onblur='removeMark(4)'/>
					</td>
				</tr>

				<tr>
					<td class='padding'>Email:</td>
					<td>
						<span class='err'></span>
						<input type='text' name='email' class='textbox' onclick='emailCheck(this,5)' onkeyup='emailCheck(this,5)' onblur='removeMark(5)'/>
					</td>
				</tr>

				<tr>
					<td class='padding'>Address:</td>
					<td>
						<span class='err'></span>
						<input type='text' name='address' class='textbox' onclick='addressCheck(this,6)' onkeyup='addressCheck(this,6)' onblur='removeMark(6)' />
					</td>

				</tr>

				<tr>
					<td class='padding'>Phone no.:</td>
					<td>
						<span class='err'></span>
						<input type='text' name='phone' class='textbox' onclick='phoneCheck(this,7)' onkeyup='phoneCheck(this,7)' onblur='removeMark(7)'/>
					</td>

				</tr>

				<tr>
					<td>
						<div onmouseover='getStatus()'>
							<input type='submit' name='submit' value='Register' class='button' id='submit' disabled='true'/>
						</div>
					</td>
					<td><input type='reset' name ='reset' value='Reset' class='button' onclick='unmarkAll()'/></td>
				</tr>
				<tr><td colspan='2' style='padding:5px' /></tr>
			</table>
		</form>
		<?php
			if($_POST):
				extract($_POST);
				$query="INSERT INTO users VALUES('$username', '$fname', '$lname', '$pass1', '$phone', '$email', '$address')";
				mysql_query($query);
				if(!mysql_affected_rows($con)):
					echo "<script>alert('Something went wrong, Try again....');</script>";
				else:
					$_SESSION['user']=$username;
					header("location:http://getnokia.tk/");
				endif;
			endif;
		?>
		<div class='gap2'></div>
		<div class='footer'>
			This site is a <span class='bold'>college project</span> made by <span class='bold'>Vimal Khullar</span>
		</div>
	</body>
</html>
