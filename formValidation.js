var err=document.getElementsByClassName('err');
function usernameCheck(me,val)
{
	var ajax, check;
	if(window.XMLHttpRequest)
		ajax=new XMLHttpRequest();
	else
		ajax=new ActiveXObject("Microsoft.XMLHTTP");
	if(me.value.length==0)
	{
		me.style.borderColor='';
		err[val].innerText="Username can't be left empty!!!";
	}
	else if(me.value.length < 5 || me.value.length >= 20)
	{
		me.style.borderColor='red';
		err[val].innerText='Username too short or too long!!!';
	}
	else
	{
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4 && ajax.status==200)
				check = ajax.responseText;
		}
		ajax.open("GET","usercheck.php?username=" + me.value,false);
		ajax.send();
		if(check == 1)
		{
			//alert(check);
			me.style.borderColor='red';
			err[val].innerText='Username already exists!!!';
		}
		else if(check == 0)
		{
			//alert(check);
			me.style.borderColor='green';
			err[val].innerText='';
		}
	}
}

function nameCheck(me,val)
{
	var name;
	if(val == 1)
		name='First';
	else
		name='Last';
	if(me.value.length == 0)
	{
		me.style.borderColor='';
		err[val].innerText=name + " name can't be left empty!!!";
	}
	else if(me.value.length < 3 || me.value.length >= 15)
	{
		err[val].innerText=name + " name too short or too long!!!"
		me.style.borderColor='red';
	}
	else
	{
		me.style.borderColor='green';
		err[val].innerText='';
	}
}

function passCheck(me,val)
{
	if(me.value.length == 0)
	{
		me.style.borderColor='';
		err[val].innerText="Password can't be left empty!!!";
	}
	else if(me.value.length <= 5 || me.value.length >= 20)
	{
		me.style.borderColor='red';
		err[val].innerText='Too short or too long!!!';
	}
	else if(me.value.length <= 8)
	{
		me.style.borderColor='orange';
		err[val].innerText='Good';
	}
	else if(me.value != document.getElementById('pass').value)
	{
		me.style.borderColor='red';
		err[val].innerText="Passwords don't match!!!";
	}
	else
	{
		me.style.borderColor='green';
		err[val].innerText='Strong';
	}
}

function emailCheck(me,val)
{
	var at=me.value.indexOf("@");
	var dot=me.value.lastIndexOf(".");
	dot=dot - at;
	if(me.value.length == 0)
	{
		me.style.borderColor='';
		err[val].innerText="Email can't be left empty!!!";
	}
	else if((at < 5) || (dot < 5))
	{
		me.style.borderColor='red';
		err[val].innerText='Email invalid!!!';
	}
	else
	{
		me.style.borderColor='green';
		err[val].innerText='';
	}
}

function addressCheck(me,val)
{
	if(me.value.length == 0)
	{
		me.style.borderColor='';
		err[val].innerText="Address can't be left empty!!!";
	}
	else if(me.value.length < 15)
	{
		me.style.borderColor='red';
		err[val].innerText='Address too short!!!';
	}
	else
	{
		me.style.borderColor='green';
		err[val].innerText='';
	}
}

function phoneCheck(me,val)
{
	if(me.value.length == 0)
	{
		me.style.borderColor='';
		err[val].innerText="Phone can't be left empty!!!";
	}
	else if(me.value.length != 10 || !Number(me.value))
	{
		me.style.borderColor='red';
		err[val].innerText='Phone no. incorrect!!!';
	}
	else
	{
		me.style.borderColor='green';
		err[val].innerText='';
	}
}

function removeMark(val)
{
	err[val].innerText='';
}

function unmarkAll()
{
	var box=document.getElementsByClassName('textbox');
	for(var i=1;i<=8;i++)
	{
		err[i-1].innerText='';
		box[i].style.borderColor='';
	}
	document.getElementById('submit').disabled='true';
}

function getStatus()
{
	button=document.getElementById('submit');
	textbox=document.getElementsByClassName('textbox');
	var count=0;
	for(var i=1;i<=8;i++)
	{
		if(textbox[i].style.borderColor == 'green')
			count += 1;
	}
	if(count == 8)
		button.disabled='';
	else
		button.disabled='true';
}

function enabler()
{
	var elt = document.getElementsByClassName('textbox');
	var err = document.getElementsByClassName('err');
	var toEnable = document.getElementById('toEnable');
	if(elt[2] == undefined)
	{
		if(elt[1].style.borderColor == 'green')
			toEnable.disabled='';
		else
			toEnable.disabled='disabled';
		err[1].innerHTML='';
	}
	else
	{
		if(elt[1].style.borderColor == 'green' && elt[2].style.borderColor == 'green')
			toEnable.disabled='';
		else
			toEnable.disabled='disabled';
		err[1].innerHTML='';
		err[2].innerHTML='';
	}
}