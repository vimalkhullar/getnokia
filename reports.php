<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Get Nokia</title>
		<script src='titlebar.js' type='text/javascript' ></script>
		<link rel='stylesheet' type='text/css' href='titlebar.css' />
		<?php
			session_start();
			require('connect.php');
			$con = connect_db();
			if($_SESSION['user'] != "admin"):	header("location:login.php");	endif;
			$day = Date('y-m-d', strtotime(date('y-m-d') . "-1 Day")) . "<br/>";
			$week = Date('y-m-d',strtotime(date('y-m-d') . "-8 Days")) . "<br/>";
			$mon = Date('y-m-d',strtotime(date('y-m-d') . "-1 Month")) . "<br/>";
			$qtr = Date('y-m-d',strtotime(date('y-m-d') . "-6 Months")) . "<br/>";
			$yr = Date('y-m-d',strtotime(date('y-m-d') . "-1 Year")) . "<br/>";
		?>
		<style type='text/css'>
			table{
				margin:auto;
				margin-top:20px;
			}

			table.report{
				width:200px;
				padding:5px;
				border-collapse:collapse;
				border:2px solid rgb(0,114,198);
			}

			th{
				background-color:rgb(0,114,198);
				padding:10px;
				font-size:large;
			}

			ul.report li{
				display:inline-block;
				padding:10px;
			}
		</style>
	</head>
	<body>
		<div class='back'>
			<div class='front'>
				<ul>
					<li style='vertical-align:initial;'><a href='http://getnokia.tk/'><img src='imgs/main.png' style='border:none;outline:none;padding-left:20px;'/></a></li>
					<li style='padding:24px 20px 24px 2in!important;vertical-align:top;'>
						Search: <input type='text' class='textbox' onkeyup='show_sr_box(this)'
						onclick='show_sr_box(this)' onmousemove='show_sr_box(this)'/>
						<div class='sr_box' id='sr_box' onmousemove="this.style.display='block';" onmouseout="this.style.display='none';"></div>
					</li>
					<li class='hover' onclick="location.href='compare.php'">Phone Fight</li>
					<?php	if(isset($_SESSION['user'])):	?>
						<li class='hover' onmouseover='show_user_ctrl()' onmousemove='show_user_ctrl()' onmouseout='hide_user_ctrl()'>
							<?php
								$res=mysql_query("SELECT fname, lname FROM users WHERE username = '" . $_SESSION['user'] . "'");
								$row=mysql_fetch_assoc($res);
								echo $_SESSION['user'] . " (" . $row['fname'] . " " . $row['lname'] . ")";
							?>

							<div class='main_box' id='main_box'	onmousemove="this.style.display='block';" onmouseout="this.style.display='none';">
									<ul>
										<li onclick="location.href='myCart.php';">Cart</li>
										<li onclick="location.href='orders.php';">Orders</li>
										<li onclick="location.href='myinfo.php';">Settings</li>
										<li onclick="location.href='fileReturn.php';">File Return</li>
										<?php	if($_SESSION['user'] === "admin"):	?>
												<li onclick="location.href='update.php';">Update</li>
												<li onclick="location.href='reports.php';">View reports</li>
										<?php endif;	?>
										<li onclick="location.href='logout.php';">Log Out</li>
									</ul>
							</div>
						</li>
					<?php else:	?>
						<li class='hover' onclick="location.href='login.php'">Login</li>
						<li class='hover' onclick="location.href='register.php'">Register</li>
					<?php endif;	?>
				</ul>
			</div>
		</div>
		<div class='gap1'></div>
		<table><tr><td><form action='' method='post'>
			<ul class='report'>
				<li>Period:
					<select name='period'>
						<option value=''>Default</option>
						<option value='week'>Week</option>
						<option value='mon'>Month</option>
						<option value='qtr'>Quarter</option>
						<option value='yr'>Year</option>
					</select>
				</li>
				<li><input type='submit' value='Generate'/></li>
			</ul>
		</form></td></tr></table>
		<?php
			if($_POST):
				if($_POST['period']):
					echo "<table style='width:10in;'>";
					echo "<tr><td><table class='report'><tr><th>GLOBAL UNITS</th></tr>";
					$res = mysql_query("SELECT SUM(qty_available) FROM phones");
					$row = mysql_fetch_assoc($res);
					$total = $row['SUM(qty_available)'];
					$query = "SELECT SUM(o.qty_order) FROM orders o, shipping s WHERE (o.shipping_id = s.shipping_id AND s.actual IS NOT NULL) AND (s.actual <='$day' AND s.actual >= '" . $$_POST['period'] . "');";
					$res = mysql_query($query);
					$row = mysql_fetch_assoc($res);
					$width = $row['SUM(o.qty_order)'];
					echo "<tr><td><ul class='panel'><li style='width:$width%'>Sold:$width</li><ul></td></tr>";
					echo "<tr><td><ul class='panel'><li style='width:100%;'>Stock remaning:$total</li></ul></td></tr></table></td>";


					$res = mysql_query("SELECT phone_name, model, qty_available FROM phones WHERE status = 'available'");
					$i=1;
					while($row = mysql_fetch_array($res))
					{
						$qry = mysql_query("SELECT SUM(o.qty_order) FROM orders o, shipping s WHERE (o.shipping_id = s.shipping_id AND s.actual IS NOT NULL) AND (s.actual <='$day' AND s.actual >= '" . $$_POST['period'] . "') AND o.phone_model = '" . $row['model'] . "'");
						$sold = mysql_fetch_assoc($qry);

						if($i == 3):
							$pre = "<tr>";
							$i = 0;
						else:
							$pre = "";
						endif;

						$i++;

						echo "$pre<td><table class='report'><tr><th>" . $row['phone_name'] . "</th></tr>";

						if($sold['SUM(o.qty_order)'] == NULL):
							echo "<tr><td><ul class='panel'><li style='width:100%;background-color:grey;'>Not sold!!!!</li></ul></td></tr>";
						else:
							$per = ($sold['SUM(o.qty_order)'] / ($row['qty_available'] + $sold['SUM(o.qty_order)'])) * 100;
							echo "<tr><td><ul class='panel'><li style='width:$per%'>Sold:" . $sold['SUM(o.qty_order)'] . "</li></ul></td></tr>";
						endif;

						echo "<tr><td><ul class='panel'><li style='width:100%'>Stock remaining:" . $row['qty_available'] . "</li></ul></td></tr>";
						echo "</table></td>";
					}
					echo "</tr></table>";
				endif;
			endif;
		?>
		<div class='gap2'></div>
		<div class='footer'>
			This site is a <span class='bold'>college project</span> made by <span class='bold'>Vimal Khullar</span>
		</div>
	</body>
</html>
