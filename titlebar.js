function show_sr_box(me)
{
	var elt=document.getElementById('sr_box');
	if(me.value == '')
	{
		elt.style.display='none';
		return;
	}
	else
	{
		get_sr(me.value);
		elt.style.display='block';
	}
}

function hide_sr_box()
{
	document.getElementById('sr_box').style.display='none';
}

function show_user_ctrl()
{
	var elt=document.getElementById('main_box');
	elt.style.display='block';
	document.getElementById('sr_box').style.display='none';
}

function hide_user_ctrl()
{
	var elt=document.getElementById('main_box');
	elt.style.display='none';
}

function get_sr(str)
{
	var ajax;
	if(window.XMLHttpRequest)
		ajax=new XMLHttpRequest();
	else
		ajax=new ActiveXObject("Microsoft.XMLHTTP");

	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4 && ajax.status==200)
			document.getElementById('sr_box').innerHTML=ajax.responseText;
	}
	ajax.open("GET","search.php?str=" + str,true);
	ajax.send();
}			