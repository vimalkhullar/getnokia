<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Get Nokia</title>
		<script src='titlebar.js' type='text/javascript' ></script>
		<link rel='stylesheet' type='text/css' href='titlebar.css' />
		<link rel='stylesheet' type='text/css' href='http://cdn.webrupee.com/font' />
		<?php
			session_start();
			require('connect.php');
			$con = connect_db();
			if(!isset($_SESSION['user'])):	header("location:login.php");	endif;
			function getFields($res)
			{
				echo "<tr>";
				while($fld = mysql_fetch_field($res))
				{
					if($fld->name === "username")
						continue;
					echo "<th class='orders'>" . ucwords(str_replace('_',' ',$fld->name)) . "</th>";
				}
				echo "</tr>";
			}
		?>

		<style type='text/css'>
			table{
				margin:auto;
			}

			table.orders{
				margin-top:50px;
				border:2px solid #0072C6;
				width:1000px;
				border-collapse:collapse;
				text-align:center;
				display:table;
			}

			table.orders th.orders{
				background-color:#0072C6;
				font-size:large;
				font-style:normal;
				font-variant:small-caps;
			}
		</style>
	</head>
	<body>
		<div class='back'>
			<div class='front'>
				<ul>
					<li style='vertical-align:initial;'><a href='http://getnokia.tk/'><img src='imgs/main.png' style='border:none;outline:none;padding-left:20px;'/></a></li>
					<li style='padding:24px 20px 24px 2in!important;vertical-align:top;'>
						Search: <input type='text' class='textbox' onkeyup='show_sr_box(this)'
						onclick='show_sr_box(this)' onmousemove='show_sr_box(this)'/>
						<div class='sr_box' id='sr_box' onmousemove="this.style.display='block';" onmouseout="this.style.display='none';"></div>
					</li>
					<li class='hover' onclick="location.href='compare.php'">Phone Fight</li>
					<?php	if(isset($_SESSION['user'])):	?>
						<li class='hover' onmouseover='show_user_ctrl()' onmousemove='show_user_ctrl()' onmouseout='hide_user_ctrl()'>
							<?php
								$res=mysql_query("SELECT fname, lname FROM users WHERE username = '" . $_SESSION['user'] . "'");
								$row=mysql_fetch_assoc($res);
								echo $_SESSION['user'] . " (" . $row['fname'] . " " . $row['lname'] . ")";
							?>

							<div class='main_box' id='main_box'	onmousemove="this.style.display='block';" onmouseout="this.style.display='none';">
									<ul>
										<li onclick="location.href='myCart.php';">Cart</li>
										<li onclick="location.href='orders.php';">Orders</li>
										<li onclick="location.href='myinfo.php';">Settings</li>
										<li onclick="location.href='fileReturn.php';">File Return</li>
										<?php	if($_SESSION['user'] === "admin"):	?>
												<li onclick="location.href='update.php';">Update</li>
												<li onclick="location.href='reports.php';">View reports</li>
										<?php endif;	?>
										<li onclick="location.href='logout.php';">Log Out</li>
									</ul>
							</div>
						</li>
					<?php else:	?>
						<li class='hover' onclick="location.href='login.php'">Login</li>
						<li class='hover' onclick="location.href='register.php'">Register</li>
					<?php endif;	?>
				</ul>
			</div>
		</div>
		<div class='gap1'></div>
		<table>
			<?php
				echo "<tr><td><table class='orders'><tr><th class='orders' colspan='10' style='font-size:x-large;padding:5px;'>ACTIVE ORDERS</th></tr>";
				$ordercount=0;
				$query="SELECT o.* , s.company, s.expected, s.actual FROM orders o, shipping s WHERE (o.shipping_id = s.shipping_id AND s.actual IS NULL) AND o.username='". $_SESSION['user'] . "' ORDER BY o.order_date DESC";
				$res=mysql_query($query);
				getFields($res);

				function getPending($res)
				{
					while($row = mysql_fetch_array($res))
					{
						echo "<tr>";
						$i=0;
						$id;
						foreach($row as $key=>$val)
						{
							if($key === $i):
								$i++;
								continue;
							elseif($key === "order_id"):
								$id="delete.php?id=$val";
							elseif($key === "username"):
								continue;
							elseif($key === "phone_model"):
								$phone=mysql_query("SELECT phone_name FROM phones WHERE model = '$val'");
								$row=mysql_fetch_array($phone);
								echo "<td>" . $row[0] . "</td>";
								continue;
							elseif($key === "actual"):
								echo "<td>Not delivered yet!!!</td>";
								continue;
							elseif($key === "total_amt"):
								echo "<td><span class='WebRupee'>Rs </span>$val</td>";
								continue;
							elseif($key === "shipping_id"):
								if(!isset($val)):
									echo "<td colspan='4'>-----------------To be shipped soon-----------------";
									echo "<input type='button' title='Cancel Order' value='X' style='border-radius:10px;float:right;padding:2px 6px 2px 6px;' onclick=location.href='" . $id . "' /></td>";
									break;
								endif;
							endif;
							echo "<td>$val</td>";
						}
						echo "</tr>";
					}
				}


				$resone=mysql_query("SELECT * FROM orders WHERE shipping_id IS NULL AND order_date IS NOT NULL ORDER BY order_date DESC");
				if(mysql_affected_rows($con)):
					getPending($resone);
					$ordercount++;
				endif;

				$res=mysql_query($query);
				if(mysql_affected_rows($con)):
					getPending($res);
					$ordercount++;
				endif;

				if($ordercount === 0):
					echo "<tr><td colspan='10' style='text-align:center;'>NO ACTIVE ORDERS!!!</td></tr>";
				endif;

				echo "</table></td></tr>";
			?>

			<?php
				echo "<tr><td><table class='orders'><tr><th colspan='10' class='orders' style='font-size:x-large;padding:5px;'>COMPLETED ORDERS</th></tr>";
				$res=mysql_query("SELECT o.* , s.company, s.expected, s.actual FROM orders o, shipping s WHERE  (o.shipping_id = s.shipping_id AND s.actual IS NOT NULL) AND o.username='". $_SESSION['user'] . "' ORDER BY o.order_date DESC");
				getFields($res);
				if(!mysql_affected_rows($con)):
					echo "<tr><td colspan='10' style='text-align:center;'>NO ORDERS!!!!</td></tr>";
				else:
					while($row = mysql_fetch_array($res))
					{
						echo "<tr>";
						$i=0;
						$oid = $row['order_id'];
						foreach($row as $key=>$val)
						{
							if($key === $i):	$i++;	continue;	endif;
							if($key === "username"):
							elseif($key === "total_amt"):
								echo "<td><span class='WebRupee'>Rs </span>$val</td>";
							elseif($key === "phone_model"):
								$phone=mysql_query("SELECT phone_name FROM phones WHERE model = '$val'");
								$row=mysql_fetch_array($phone);
								echo "<td>" . $row[0] . "</td>";
							elseif($key === "actual"):
								echo "<td>$val";
								echo "<input type='button' title='Download Invoice' value='&darr;' style='float:right;border-radius:10px;padding:0px 5px 2px 6px;' onclick=location.href='invoice.php?oid=$oid'; /></td>";
							else:
								echo "<td>$val</td>";
							endif;

						}
						echo "</tr>";
					}
				endif;
				echo "</table></td></tr>";
			?>
		</table>
		<div class='gap2'></div>
		<div class='footer'>
			This site is a <span class='bold'>college project</span> made by <span class='bold'>Vimal Khullar</span>
		</div>
	</body>
</html>
