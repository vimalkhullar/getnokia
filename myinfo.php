<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Get Nokia</title>
		<script src='titlebar.js' type='text/javascript' ></script>
		<link rel='stylesheet' type='text/css' href='titlebar.css' />
		<script src='formValidation.js' type='text/javascript'></script>
		<?php
			session_start();
			if(!isset($_SESSION['user'])):	header("location:error.php"); endif;
			require('connect.php');
			$con = connect_db();
			if(!isset($_SESSION['user'])):	header("location:login.php");	endif;
			function getInfo($val)
			{
				global $con;
				$query="SELECT $val FROM users WHERE username='" . $_SESSION['user'] . "'";
				$res=mysql_query($query);
				if(!mysql_affected_rows($con)): header("location:error.php"); endif;
				$row=mysql_fetch_array($res);
				echo $row[$val];
			}
		?>

		<style type='text/css'>
			table{
				margin:auto;
				margin-top:50px;
				border:2px solid rgb(0,114,198);
				border-collapse:collapse;
			}

			table td{
				padding:5px 5px 5px 20px;
			}

			.head{
				background-color:rgb(0,114,198);
				font-size:x-large;
				padding:10px;
			}
		</style>
	</head>
	<body>
		<div class='back'>
			<div class='front'>
				<ul>
					<li style='vertical-align:initial;'><a href='http://getnokia.tk/'><img src='imgs/main.png' style='border:none;outline:none;padding-left:20px;'/></a></li>
					<li style='padding:24px 20px 24px 2in!important;vertical-align:top;'>
						Search: <input type='text' class='textbox' onkeyup='show_sr_box(this)'
						onclick='show_sr_box(this)' onmousemove='show_sr_box(this)'/>
						<div class='sr_box' id='sr_box' onmousemove="this.style.display='block';" onmouseout="this.style.display='none';"></div>
					</li>
					<li class='hover' onclick="location.href='compare.php'">Phone Fight</li>
					<?php	if(isset($_SESSION['user'])):	?>
						<li class='hover' onmouseover='show_user_ctrl()' onmousemove='show_user_ctrl()' onmouseout='hide_user_ctrl()'>
							<?php
								$res=mysql_query("SELECT fname, lname FROM users WHERE username = '" . $_SESSION['user'] . "'");
								$row=mysql_fetch_assoc($res);
								echo $_SESSION['user'] . " (" . $row['fname'] . " " . $row['lname'] . ")";
							?>

							<div class='main_box' id='main_box'	onmousemove="this.style.display='block';" onmouseout="this.style.display='none';">
									<ul>
										<li onclick="location.href='myCart.php';">Cart</li>
										<li onclick="location.href='orders.php';">Orders</li>
										<li onclick="location.href='myinfo.php';">Settings</li>
										<li onclick="location.href='fileReturn.php';">File Return</li>
										<?php	if($_SESSION['user'] === "admin"):	?>
												<li onclick="location.href='update.php';">Update</li>
												<li onclick="location.href='reports.php';">View reports</li>
										<?php endif;	?>
										<li onclick="location.href='logout.php';">Log Out</li>
									</ul>
							</div>
						</li>
					<?php else:	?>
						<li class='hover' onclick="location.href='login.php'">Login</li>
						<li class='hover' onclick="location.href='register.php'">Register</li>
					<?php endif;	?>
				</ul>
			</div>
		</div>
		<div class='gap1'></div>
		<?php if(!$_POST && !$_GET):	?>
		<form action='' method='post'>
			<table>
				<tr>
					<th colspan='3' class='head'><span style='font-variant:small-caps;'>Username:</span><span style='text-decoration:underline;padding-left:10px;'><?php getInfo('username');	?></span></th>
				</tr>
				<tr>
					<td>Password:</td>
					<td>*****</td>
					<td><input type='submit' name='password' value='Change'/>
				</tr>
				<tr>
					<td>Name:</td>
					<td>
						<?php
							getInfo('fname');
							echo " ";
							getInfo('lname');
						?>
					</td>
					<td><input type='submit' name='name' value='Change'/>
				</tr>
				<tr>
					<td>Address:</td>
					<td><?php getInfo('address');	?></td>
					<td><input type='submit' name='address' value='Change'/>
				</tr>
				<tr>
					<td>Email:</td>
					<td><?php getInfo('email');	?></td>
					<td><input type='submit' name='email' value='Change'/>
				</tr>
				<tr>
					<td>Phone no.:</td>
					<td><?php getInfo('phone');	?></td>
					<td><input type='submit' name='phone' value='Change'/>
				</tr>
			</table>
		</form>
		<?php
			elseif($_POST):
				echo "<form action='' method='get'><table>";
				$subname;
				if(isset($_POST['password'])):
					echo "<tr><td>New Password:</td>";
					echo "<td><span class='err'></span>";
					echo "<input type='password' class='textbox' name='pass1' id='pass' onkeyup='passCheck(this,0)' onblur='enabler()' /></td></tr>";
					echo "<tr><td>Confirm Password:</td>";
					echo "<td><span class='err'></span>";
					echo "<input type='password' class='textbox' name='pass2' onkeyup='passCheck(this,1)' onblur='enabler()' /></td></tr>";
					$subname='password';
				elseif(isset($_POST['name'])):
					echo "<tr><td>First Name:</td>";
					echo "<td><span class='err'></span><input type='text' class='textbox' name='fname' onkeyup='nameCheck(this,0)' onblur='enabler()' /></td></tr>";
					echo "<tr><td>Last Name:</td>";
					echo "<td><span class='err'></span><input type='text' class='textbox' name='lname' onkeyup='nameCheck(this,1)' onblur='enabler()' /></td></tr>";
					$subname='name';
				elseif(isset($_POST['address'])):
					echo "<tr><td>Address:</td>";
					echo "<td><span class='err'></span>";
					echo "<textarea name='address' rows='2' cols='16' class='textbox' style='resize:none' onkeyup='addressCheck(this,0)' onblur='enabler()' ></textarea></td></tr>";
					$subname='addr';
				elseif(isset($_POST['phone'])):
					echo "<tr><td>Phone no.:</td>";
					echo "<td><span class='err'></span><input type='text' class='textbox' name='phone' onkeyup='phoneCheck(this,0)' onblur='enabler()' /></td></tr>";
					$subname='fone';
				elseif(isset($_POST['email'])):
					echo "<tr><td>Email:</td>";
					echo "<td><span class='err'></span><input type='text' class='textbox' name='email' onkeyup='emailCheck(this,0)' onblur='enabler()' /></td></tr>";
					$subname='e-mail';
				endif;
				echo "<tr><td style='padding-top:30px;'>Current Password:</td>";
				echo "<td style='padding-top:30px;'><input type='password' id='textbox' name='pass' /></td></tr>";
				echo "<tr><td colspan='2' style='text-align:center;'><div id='enabler' onmouseover='enabler()' style='display:inline;padding-right:10px;'><input type='submit' name='$subname' value='Change' id='toEnable' class='button' disabled='disabled' /></div>";
				echo "<input type='button' value='Back' class='button' onclick=location.href='myinfo.php' /></td></tr>";
				echo "</form></table>";

			elseif($_GET):
				$query="SELECT username FROM users WHERE password='" . $_GET['pass'] ."' AND username='" . $_SESSION['user'] . "'";
				mysql_query($query);
				if(!mysql_affected_rows($con)):
					echo "<script>alert('Password incorrect');location.href='myinfo.php';</script>";
					break;
				endif;
				$q;
				if(isset($_GET['password'])):
					$q="password='" . $_GET['pass1'] . "'";
				elseif(isset($_GET['name'])):
					$q="fname='" . $_GET['fname'] . "', lname='" . $_GET['lname'] . "'";
				elseif(isset($_GET['addr'])):
					$q="address='" . $_GET['address'] . "'";
				elseif(isset($_GET['fone'])):
					$q="phone='" . $_GET['phone'] . "'";
				elseif(isset($_GET['e-mail'])):
					$q="email='" . $_GET['email'] . "'";
				endif;
				$query="UPDATE users SET $q WHERE username='" . $_SESSION['user'] . "'";
				mysql_query($query);
				if(!mysql_affected_rows($con)):
					echo "<script>alert('Not able to update your data ATM');";
				else:
					echo "<script>alert('Changes made successfully');";
				endif;
				echo "location.href='myinfo.php';</script>";
			endif;
		?>
		<div class='gap2'></div>
		<div class='footer'>
			This site is a <span class='bold'>college project</span> made by <span class='bold'>Vimal Khullar</span>
		</div>
	</body>
</html>
