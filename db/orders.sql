-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 16, 2013 at 06:10 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `windowsfone`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(5) NOT NULL DEFAULT '0',
  `username` varchar(20) DEFAULT NULL,
  `phone_model` varchar(20) DEFAULT NULL,
  `qty_order` int(2) DEFAULT NULL,
  `total_amt` varchar(7) DEFAULT NULL,
  `order_date` datetime DEFAULT NULL,
  `mode` varchar(20) DEFAULT 'COD',
  `shipping_id` int(5) DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `username`, `phone_model`, `qty_order`, `total_amt`, `order_date`, `mode`, `shipping_id`) VALUES
(24919, 'admin', 'lm920', 1, '23,499', '2013-11-16 22:38:53', 'COD', 25836),
(26581, 'admin', 'lm720', 20, '329,980', '2013-11-14 20:01:20', 'COD', 13214),
(30646, 'user2', 'lm625', 1, '14,275', '2013-11-02 18:46:08', 'COD', 11358),
(53676, 'admin', 'lm925', 1, '27,500', '2013-11-02 20:50:50', 'COD', 96321),
(58738, 'user2', 'lm1020', 1, '49,000', '2013-11-02 19:17:18', 'COD', 35412),
(61374, 'admin', 'lm1020', 2, '98,000', '2013-11-16 23:02:49', 'COD', NULL),
(72141, 'admin', 'lm520', 1, '8,299', NULL, 'COD', NULL),
(80265, 'newuser', 'lm520', 1, '8,299', '2013-11-02 13:25:23', 'COD', 11122),
(84006, 'admin', 'lm620', 1, '12,399', NULL, 'COD', NULL),
(84380, 'newuser', 'lm625', 2, '28,550', '2013-11-02 13:24:22', 'COD', 62134),
(87066, 'admin', 'lm625', 4, '57,100', '2013-11-16 22:09:33', 'COD', 14725);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
