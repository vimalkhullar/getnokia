-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 16, 2013 at 06:10 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `windowsfone`
--

-- --------------------------------------------------------

--
-- Table structure for table `shipping`
--

CREATE TABLE IF NOT EXISTS `shipping` (
  `shipping_id` int(5) NOT NULL DEFAULT '0',
  `company` varchar(30) DEFAULT 'FedEx',
  `expected` date DEFAULT NULL,
  `actual` date DEFAULT NULL,
  PRIMARY KEY (`shipping_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shipping`
--

INSERT INTO `shipping` (`shipping_id`, `company`, `expected`, `actual`) VALUES
(11122, 'FedEx', '2013-11-08', '2013-11-03'),
(11358, 'FedEx', '2013-11-10', '2013-11-12'),
(13214, 'FedEx', '2013-11-14', '2013-11-12'),
(14725, 'FedEx', '2013-11-20', '2013-11-18'),
(25836, 'FedEx', '2013-11-20', NULL),
(35412, 'FedEx', '2013-11-10', '2013-11-12'),
(62134, 'FedEx', '2013-11-03', '2013-11-02'),
(96321, 'FedEx', '2013-11-14', '2013-11-12');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
