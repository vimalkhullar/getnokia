-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 16, 2013 at 06:10 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `windowsfone`
--

-- --------------------------------------------------------

--
-- Table structure for table `phones`
--

CREATE TABLE IF NOT EXISTS `phones` (
  `model` varchar(20) NOT NULL,
  `phone_name` varchar(30) DEFAULT NULL,
  `released` date DEFAULT NULL,
  `qty_available` int(2) DEFAULT NULL,
  `price` varchar(6) DEFAULT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'Available',
  `sim` char(10) DEFAULT 'Micro-SIM',
  `dim` varchar(20) DEFAULT NULL,
  `weight` int(3) DEFAULT NULL,
  `screen` varchar(30) DEFAULT NULL,
  `size` float(3,1) DEFAULT NULL,
  `res` varchar(20) DEFAULT '480 x 800 pixels',
  `protect` char(40) DEFAULT NULL,
  `ram` int(4) DEFAULT NULL,
  `internal` int(2) DEFAULT NULL,
  `card` varchar(15) DEFAULT NULL,
  `wlan` varchar(30) DEFAULT NULL,
  `bt` varchar(15) DEFAULT NULL,
  `nfc` char(3) NOT NULL DEFAULT 'No',
  `usb` varchar(30) DEFAULT 'Yes, microUSB v2.0',
  `main` varchar(50) DEFAULT NULL,
  `addl` tinytext,
  `video` varchar(20) DEFAULT NULL,
  `secondary` varchar(30) DEFAULT NULL,
  `os` varchar(30) DEFAULT 'Microsoft Windows Phone 8',
  `cpu` varchar(30) DEFAULT NULL,
  `gpu` varchar(30) DEFAULT NULL,
  `chip` varchar(30) DEFAULT NULL,
  `power` int(4) DEFAULT NULL,
  `stand` varchar(50) DEFAULT NULL,
  `talk` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`model`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `phones`
--

INSERT INTO `phones` (`model`, `phone_name`, `released`, `qty_available`, `price`, `status`, `sim`, `dim`, `weight`, `screen`, `size`, `res`, `protect`, `ram`, `internal`, `card`, `wlan`, `bt`, `nfc`, `usb`, `main`, `addl`, `video`, `secondary`, `os`, `cpu`, `gpu`, `chip`, `power`, `stand`, `talk`) VALUES
('asha500', 'Nokia Asha 500', NULL, 0, '4,500', 'Coming soon', 'Micro-SIM', '3.95 x 2.29 x 0.50', 101, 'TFT capacitive touchscreen', 2.8, '240 x 320 pixels', 'Yes', 64, 0, 'Yes, upto 32 GB', 'Wi-Fi 802.11 b/g/n', 'Yes, v3.0', 'No', 'Yes, microUSB v2.0', '2 MP, 1600 x 1200 pixels', 'N/A', 'QVGA@13fps', 'No', 'Nokia Asha 1.1.1', 'N/A', 'N/A', 'N/A', 1200, 'Up to 840 h', 'Up to 14 h'),
('asha502', 'Nokia Asha 502', '0000-00-00', 0, '5,500', 'Coming soon', 'Micro-SIM', '3.92 x 2.34 x 0.44', 100, 'TFT capacitive touchscreen', 3.0, '240 x 320 pixels', 'Yes', 64, 0, 'Yes, upto 32 GB', 'Wi-Fi 802.11 b/g/n', 'Yes, v3.0', 'No', 'Yes, microUSB v2.0', '5 MP, 2592 x 1944 pixels', 'LED flash', 'QVGA@15fps', 'No', 'Nokia Asha 1.1', 'N/A', 'N/A', 'N/A', 1010, 'Up to 576 h', 'Up to 13 h 42 min'),
('lm1020', 'Nokia Lumia 1020', '2013-07-01', 20, '49,000', 'Available', 'Micro-SIM', '5.13 x 2.81 x 0.41', 158, 'AMOLED capacitive touchscreen ', 4.5, '768 x 1280 pixels', 'Corning Gorilla Glass 3', 2048, 32, 'No', 'Wi-Fi 802.11 a/b/g/n', 'Yes, v3.0', 'Yes', 'Yes, microUSB v2.0', '41 MP (38 MP effective, 7152 x 5368 pixels)', 'Carl Zeiss optics, optical image stabilization, auto/manual focus, Xenon & LED flash, 1/1.5'''' sensor size, 1.12 µm pixel size, PureView technology, geo-tagging, face detection, dual capture, panorama', 'Yes, 1080p@30fps', 'Yes, 1.2 MP, 720p@30fps', 'Microsoft Windows Phone 8', 'Dual-core 1.5 GHz Krait', 'Adreno 225', 'Qualcomm MSM8960 Snapdragon', 2000, 'Up to 384 h', 'Up to 19 h (2G) / Up to 13 h 20 min (3G)'),
('lm1320', 'Nokia Lumia 1320', NULL, 0, '24,000', 'Coming soom', 'Micro-SIM', '6.46 x 3.38 x 0.39', 220, 'IPS LCD capacitive touchscreen', 6.0, '720 x 1280 pixels', 'Corning Gorilla Glass 3', 1024, 8, 'Yes, upto 64 GB', 'Wi-Fi 802.11 b/g/n', 'Yes, v4.0', 'No', 'Yes, microUSB v2.0', '5 MP, 2592 x 1944 pixels', '1/4'''' sensor size, geo-tagging, touch focus, autofocus, LED flash', 'Yes, 1080p@30fps', 'Yes, VGA', 'Microsoft Windows Phone 8', 'Dual-core 1.7 GHz', 'Adreno 305', 'Qualcomm Snapdragon S4', 3400, 'Up to 672 h', 'Up to 25 h (2G) / Up to 21 h (3G)'),
('lm1520', 'Nokia Lumia 1520', NULL, 0, '50,000', 'Coming soon', 'Nano-SIM', '6.41 x 3.36 x 0.34', 209, 'IPS LCD capacitive touchscreen', 6.0, '1080 x 1920 pixels', 'Corning Gorilla Glass 2', 2048, 32, 'Yes, upto 64 GB', 'Wi-Fi 802.11 a/b/g/n/ac', 'Yes, v4.0', 'Yes', 'Yes, microUSB v2.0', '20 MP, 7152 x 5368 pixels', 'Carl Zeiss optics, optical image stabilization, autofocus, dual-LED flash, 1/2.5'''' sensor size, PureView technology, dual capture, geo-tagging, face detection, panorama', 'Yes, 1080p@30fps', 'Yes, 1.2 MP, 720p@30fps', 'Microsoft Windows Phone 8', 'Quad-core 2.2 GHz Krait', 'Adreno 330', 'Qualcomm MSM8974 Snapdragon', 3400, 'Up to 768 h', 'Up to 27 h 40 min (2G) / Up to 25 h (3G)'),
('lm520', 'Nokia Lumia 520', '2013-04-01', 14, '8,299', 'Available', 'Micro-SIM', '4.72 x 2.52 x 0.39', 124, 'IPS LCD capacitive touchscreen', 4.0, '480 x 800 pixels', 'Scratch-resistant glass', 512, 8, 'Yes, upto 64 GB', 'Wi-Fi 802.11 a/b/g/n', 'Yes, v4.0', 'No', 'Yes, microUSB v2.0', '5 MP, 2592 x 1936 pixels', 'Autofocus, 1/4'''' sensor size, geo-tagging', 'Yes, 720p@30fps', 'No', 'Microsoft Windows Phone 8', 'Dual-core 1 GHz', 'Adreno 305', 'Qualcomm MSM8227', 1430, 'Up to 360 h', 'Up to 14 h 40 min (2G) / Up to 9 h 40 min (3G)'),
('lm620', 'Nokia Lumia 620', '2013-01-01', 17, '12,399', 'Available', 'Micro-SIM', '4.54 x 2.41 x 0.43', 127, 'TFT capacitive touchscreen', 3.8, '480 x 800 pixels', 'No', 512, 8, 'Yes, upto 64 GB', 'Wi-Fi 802.11 a/b/g/n', 'Yes, v3.0', 'Yes', 'Yes, microUSB v2.0', '5 MP, 2592 x 1936 pixels', 'Autofocus, LED flash, geo-tagging', 'Yes, 720p@30fps', 'Yes, VGA', 'Microsoft Windows Phone 8', 'Dual-core 1 GHz Krait', 'Adreno 305', 'Qualcomm Snapdragon S4', 1300, 'Up to 330 h', 'Up to 14 h 40 min (2G) / Up to 9 h 50 min (3G)'),
('lm625', 'Nokia Lumia 625', '2013-08-01', 9, '14,275', 'Available', 'Micro-SIM', '5.25 x 2.85 x 0.36', 159, 'IPS LCD capacitive touchscreen', 4.7, '480 x 800 pixels', 'Corning Gorilla Glass 2', 512, 8, 'Yes, upto 64 GB', 'Wi-Fi 802.11 b/g/n', 'Yes, v4.0', 'No', 'Yes, microUSB v2.0', '5 MP, 2592 x 1936 pixels', 'Geo-tagging, touch focus, autofocus, LED flash', 'Yes, 1080p@30fps', 'Yes, VGA', 'Microsoft Windows Phone 8', 'Dual-core 1.2 GHz Krait', 'Adreno 305', 'Qualcomm MSM8930 Snapdragon', 2000, 'Up to 552 h', 'Up to 24 h (2G) / Up to 15 h 10 min (3G)'),
('lm720', 'Nokia Lumia 720', '2013-04-01', 20, '16,499', 'Available', 'Micro-SIM', '5.04 x 2.66 x 0.35', 128, 'IPS LCD capacitive touchscreen', 4.3, '480 x 800 pixels', 'Corning Gorilla Glass 2', 512, 8, 'Yes, upto 64 GB', 'Wi-Fi 802.11 a/b/g/n', 'Yes, v3.0', 'Yes', 'Yes, microUSB v2.0', '6.1 MP, 2848 x 2144 pixels', 'Carl Zeiss optics, autofocus, LED flash, 1/3.6'''' sensor size, geo-tagging', 'Yes, 720p@30fps', 'Yes, 1.3 MP, 720p@30fps', 'Microsoft Windows Phone 8', 'Dual-core 1 GHz', 'Adreno 305', 'Qualcomm MSM8227', 2000, 'Up to 520 h', 'Up to 23 h 20 min (2G) / Up to 13 h 20 min (3G)'),
('lm820', 'Nokia Lumia 820', '2012-11-01', 18, '20,299', 'Available', 'Micro-SIM', '4.87 x 2.70 x 0.39', 160, 'AMOLED capacitive touchscreen ', 4.3, '480 x 800 pixels', 'No', 1024, 8, 'Yes, upto 64 GB', 'Wi-Fi 802.11 a/b/g/n', 'Yes, v3.1 with ', 'Yes', 'Yes, microUSB v2.0', '8 MP, 3264 x 2448 pixels', 'Carl Zeiss optics, autofocus, dual-LED flash, geo-tagging, touch focus', 'Yes, 1080p@30fps', 'Yes, VGA', 'Microsoft Windows Phone 8', 'Dual-core 1.5 GHz Krait', 'Adreno 225', 'Qualcomm MSM8960 Snapdragon', 1650, 'Up to 330 h', 'Up to 14 h (2G) / Up to 8 h (3G)'),
('lm920', 'Nokia Lumia 920', '2012-11-01', 16, '23,499', 'Available', 'Micro-SIM', '5.13 x 2.79 x 0.42', 185, 'IPS LCD capacitive touchscreen', 4.5, '768 x 1280 pixels', 'Corning Gorilla Glass 2', 1024, 32, 'No', 'Wi-Fi 802.11 a/b/g/n', 'Yes, v3.1', 'Yes', 'Yes, microUSB v2.0', '8 MP, 3264 x 2448 pixels', 'Carl Zeiss optics, optical image stabilization, autofocus, dual-LED flash, PureView technology, geo-tagging, touch focus', 'Yes, 1080p@30fps', 'Yes, 1.3 MP, 720p@30fps', 'Microsoft Windows Phone 8', 'Dual-core 1.5 GHz Krait', 'Adreno 225', 'Qualcomm MSM8960 Snapdragon', 2000, 'Up to 400 h', 'Up to 17 h (2G) / Up to 10 h (3G)'),
('lm925', 'Nokia Lumia 925', '2013-06-01', 19, '27,500', 'Available', 'Micro-SIM', '5.08 x 2.78 x 0.33', 139, 'AMOLED capacitive touchscreen ', 4.5, '768 x 1280 pixels', 'Corning Gorilla Glass 2', 1024, 32, 'No', 'Wi-Fi 802.11 a/b/g/n', 'Yes, v3.0', 'Yes', 'Yes, microUSB v2.0', '8 MP, 3264 x 2448 pixels', 'Carl Zeiss optics, optical image stabilization, autofocus, dual-LED flash, 1/3'''' sensor size, PureView technology, geo-tagging, touch focus', 'Yes, 1080p@30fps', 'Yes, 1.3 MP, 720p@30fps', 'Microsoft Windows Phone 8', 'Dual-core 1.5 GHz Krait', 'Adreno 225', 'Qualcomm MSM8960 Snapdragon', 2000, 'Up to 440 h', 'Up to 18 h 20 min (2G) / Up to 12 h 40 min (3G)');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
