<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Get Nokia</title>
		<script src='titlebar.js' type='text/javascript' ></script>
		<link rel='stylesheet' type='text/css' href='titlebar.css' />
		<?php
			session_start();
			require('connect.php');
			$con = connect_db();

			function loadContent($cap)
			{
				global $res,$con;
				if(!mysql_affected_rows($con)):	return;	endif;
				echo "<table class='main'><tr><th colspan='4'>$cap</th></tr>";
				$j=0;
				$j = mysql_affected_rows($con)>=4? 4 : mysql_affected_rows($con);
				for($i=0;$i<$j;$i++)
				{
					$row=mysql_fetch_array($res);
					extract($row);
					echo "<td class='animate'>";
					echo "<a href='mobiles.php?model=$model'>";
					echo "<table class='main2'><tr>";
					echo "<td class='img' style=" . '"background-image:url(' . "'imgs/$model/$model.jpg')" .  '"' . "></td>";
					echo "</tr><tr><td>$phone_name</td>";
					echo "</tr></table></a></td>";
				}
				echo "</tr></table>";
			}
		?>
		<style type='text/css'>
			table.main{
				border:2px solid #0072C6;
				margin:auto;
				margin-top:50px;
				width:10in;
				height:3in;
				border-collapse:collapse;
				text-align:center;
			}

			table.main table.main2{
				text-align:center;
				margin:auto;
				width:80%;
				padding:20px;
			}

			table.main th{
				background-color:#0072C6;
				color:white;
				font-size:large;
				padding:2px;
			}

			table.main .img{
				height:175px;
				width:100px;
				background-repeat:no-repeat;
				background-position:center;
				background-size:contain;
			}

			table.main .animate{
				transition: width 0.2s;
				transition: width 0.2s, height 0.2s, transform 0.2s;
				-webkit-transition: width 0.2s;
				-webkit-transition: width 0.2s, height 0.2s, -webkit-transform 0.2s;
			}

			table.main .animate:hover{
				-moz-transform: scale(1.1,1.1);
				-webkit-transform: scale(1.1,1.1);
				-o-transform: scale(1.1,1.1);
				-ms-transform: scale(1.1,1.1);
				transform: scale(1.1,1.1);
			}
		</style>
	</head>
	<body>
		<div class='back'>
			<div class='front'>
				<ul>
					<li style='vertical-align:initial;'><a href='http://getnokia.tk/'><img src='imgs/main.png' style='border:none;outline:none;padding-left:20px;'/></a></li>
					<li style='padding:24px 20px 24px 2in!important;vertical-align:top;'>
						Search: <input type='text' class='textbox' onkeyup='show_sr_box(this)'
						onclick='show_sr_box(this)' onmousemove='show_sr_box(this)'/>
						<div class='sr_box' id='sr_box' onmousemove="this.style.display='block';" onmouseout="this.style.display='none';"></div>
					</li>
					<li class='hover' onclick="location.href='compare.php'">Phone Fight</li>
					<?php	if(isset($_SESSION['user'])):	?>
						<li class='hover' onmouseover='show_user_ctrl()' onmousemove='show_user_ctrl()' onmouseout='hide_user_ctrl()'>
							<?php
								$res=mysql_query("SELECT fname, lname FROM users WHERE username = '" . $_SESSION['user'] . "'");
								$row=mysql_fetch_assoc($res);
								echo $_SESSION['user'] . " (" . $row['fname'] . " " . $row['lname'] . ")";
							?>

							<div class='main_box' id='main_box'	onmousemove="this.style.display='block';" onmouseout="this.style.display='none';">
									<ul>
										<li onclick="location.href='myCart.php';">Cart</li>
										<li onclick="location.href='orders.php';">Orders</li>
										<li onclick="location.href='myinfo.php';">Settings</li>
										<li onclick="location.href='fileReturn.php';">File Return</li>
										<?php	if($_SESSION['user'] === "admin"):	?>
												<li onclick="location.href='update.php';">Update</li>
												<li onclick="location.href='reports.php';">View reports</li>
										<?php endif;	?>
										<li onclick="location.href='logout.php';">Log Out</li>
									</ul>
							</div>
						</li>
					<?php else:	?>
						<li class='hover' onclick="location.href='login.php'">Login</li>
						<li class='hover' onclick="location.href='register.php'">Register</li>
					<?php endif;	?>
				</ul>
			</div>
		</div>
		<div class='gap1'></div>
		<?php
			//new phones
			$query="SELECT phone_name,model FROM phones ORDER BY released DESC";
			$res=mysql_query($query);
			loadContent("NEW PHONES");

			//popular phones
			$query="SELECT phones.phone_name, popularity.model, popularity.counter FROM popularity INNER JOIN phones ON popularity.model = phones.model ORDER BY popularity.counter DESC";
			$res=mysql_query($query);
			loadContent("POPULAR PHONES");

			//upcoming phones
			$query="SELECT phone_name,model FROM phones WHERE status <> 'available'";
			$res=mysql_query($query);
			loadContent("UPCOMING PHONES");
		?>
		<div class='gap2'></div>
		<div class='footer'>
			This site is a <span class='bold'>college project</span> made by <span class='bold'>Vimal Khullar</span>
		</div>

                <!-- Start of StatCounter Code for Default Guide -->
                <script type="text/javascript">
                     var sc_project=9418881;
                     var sc_invisible=0;
                     var sc_security="a4e1cb9c";
                     var scJsHost = (("https:" == document.location.protocol) ?
                     "https://secure." : "http://www.");
                     document.write("<sc"+"ript type='text/javascript' src='" +
                     scJsHost+
                     "statcounter.com/counter/counter.js'></"+"script>");
                </script>
                <noscript><div class="statcounter"><a title="free hit
                counters" href="http://statcounter.com/free-hit-counter/"
                target="_blank"><img class="statcounter"
                src="http://c.statcounter.com/9418881/0/a4e1cb9c/0/"
                alt="free hit counters"></a></div></noscript>
                <!-- End of StatCounter Code for Default Guide -->
	</body>
</html>
