<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Get Nokia</title>
		<script src='titlebar.js' type='text/javascript' ></script>
		<link rel='stylesheet' type='text/css' href='titlebar.css' />
		<?php
			session_start();
			if(!isset($_SESSION['user'])):	header("location:error.php"); endif;
			require('connect.php');
			$con = connect_db();
			if($_SESSION['user'] != "admin"):	header("location:login.php");	endif;
		?>
		<script type='text/javascript'>
			function get()
			{
				var table=document.getElementById('table').value;
				var op=document.getElementById('op').value;
				var name=document.getElementById('name').value;
				document.getElementById('go').disabled = '';
				if(table != "")
					document.getElementById("go").style.display='';
				else
					document.getElementById("go").style.display='none';

				if(table == "phones")
				{
					document.getElementById('shipp').style.display='none';
					document.getElementById('opp').style.display='';
					if(op != "add" && op != "")
						document.getElementById('phones').style.display='';
					else
						document.getElementById('phones').style.display='none';
				}
				else if(table == "shipping")
				{
					document.getElementById('phones').style.display='none';
					document.getElementById('opp').style.display='';
					if(op != 'add' && op != "")
						document.getElementById('shipp').style.display='';
					else
						document.getElementById('shipp').style.display='none';
					if(op == 'mod' && document.getElementById('ship').value == "")
						document.getElementById('go').disabled = 'true';
				}
				else if(table == "stock")
				{
					document.getElementById('phones').style.display='';
					document.getElementById('opp').style.display='none';
					document.getElementById('shipp').style.display='none';
				}
				else
				{
					document.getElementById('opp').style.display='none';
					document.getElementById('phones').style.display='none';
					document.getElementById('shipp').style.display='none';
				}
			}
		</script>
		<style type='text/css'>
			table{
				margin:auto;
				margin-top:50px;
				width:500px;
				border:2px solid rgb(0,114,198);
				border-collapse:collapse;
				text-align:center;
			}

			table th{
				background-color:rgb(0,114,198);
			}

			table td,th{
				padding:0px 5px 0px 5px;
			}
		</style>
	</head>
	<body>
		<div class='back'>
			<div class='front'>
				<ul>
					<li style='vertical-align:initial;'><a href='http://getnokia.tk/'><img src='imgs/main.png' style='border:none;outline:none;padding-left:20px;'/></a></li>
					<li style='padding:24px 20px 24px 2in!important;vertical-align:top;'>
						Search: <input type='text' class='textbox' onkeyup='show_sr_box(this)'
						onclick='show_sr_box(this)' onmousemove='show_sr_box(this)'/>
						<div class='sr_box' id='sr_box' onmousemove="this.style.display='block';" onmouseout="this.style.display='none';"></div>
					</li>
					<li class='hover' onclick="location.href='compare.php'">Phone Fight</li>
					<?php	if(isset($_SESSION['user'])):	?>
						<li class='hover' onmouseover='show_user_ctrl()' onmousemove='show_user_ctrl()' onmouseout='hide_user_ctrl()'>
							<?php
								$res=mysql_query("SELECT fname, lname FROM users WHERE username = '" . $_SESSION['user'] . "'");
								$row=mysql_fetch_assoc($res);
								echo $_SESSION['user'] . " (" . $row['fname'] . " " . $row['lname'] . ")";
							?>

							<div class='main_box' id='main_box'	onmousemove="this.style.display='block';" onmouseout="this.style.display='none';">
									<ul>
										<li onclick="location.href='myCart.php';">Cart</li>
										<li onclick="location.href='orders.php';">Orders</li>
										<li onclick="location.href='myinfo.php';">Settings</li>
										<li onclick="location.href='fileReturn.php';">File Return</li>
										<?php	if($_SESSION['user'] === "admin"):	?>
												<li onclick="location.href='update.php';">Update</li>
												<li onclick="location.href='reports.php';">View reports</li>
										<?php endif;	?>
										<li onclick="location.href='logout.php';">Log Out</li>
									</ul>
							</div>
						</li>
					<?php else:	?>
						<li class='hover' onclick="location.href='login.php'">Login</li>
						<li class='hover' onclick="location.href='register.php'">Register</li>
					<?php endif;	?>
				</ul>
			</div>
		</div>
		<div class='gap1'></div>
		<form action='' method='post'>
			<table style='width:auto!important;'>
				<tr>
					<td>
						Update:
						<select id='table' name='table' onchange='get()'>
							<option value=''>Default</option>
							<option value='phones'>Phones</option>
							<option value='shipping'>Shipping</option>
							<option value='stock'>Stock</option>
						</select>
					</td>
					<td>
						<div id='opp' style='display:none;'>
							Operation:
							<select id='op' name='op' onchange='get()'>
								<option value='add'>ADD</option>
								<option value='mod'>MODIFY</option>
								<option value='del' disabled='disabled'>DELETE</option>
							</select>
						</div>
					</td>
					<td>
						<div id='phones' style='display:none;'>
							Phone Name:
							<select id='name' name='name' onchange='get()'>
								<?php
									$res=mysql_query("SELECT phone_name FROM phones");
									while($row=mysql_fetch_array($res))
										echo "<option value='$row[0]'>$row[0]</option>";
								?>
							</select>
						</div>
					</td>
					<td>
						<div id='shipp' style='display:none;'>
							Shipping Id:
							<select id='ship' name='ship' onchange='get()'>
								<?php
									$res=mysql_query("SELECT shipping_id FROM shipping WHERE actual IS NULL");
									if(mysql_affected_rows($con)):
										while($row=mysql_fetch_array($res))
											echo "<option value='$row[0]'>$row[0]</option>";
									else:
										echo "<option value=''>None</option>";
									endif;
								?>
							</select>
						</div>
					</td>
					<td><input type='submit' value='Go' name='go' id='go' style='border:none;display:none;solid grey;border-radius:20px;' /></td>
				</tr>
			</table>
		</form>
		<div id='update'>
			<?php
				if($_POST):
					extract($_POST);
					if($table == 'phones'):
						if($op == 'add'):
							$query="DESCRIBE $table";
							$i = 0;
							$res=mysql_query($query);
							echo "<form action='' method='get'><table>";
							while($row = mysql_fetch_array($res))
							{
								echo "<tr><td>" . ucwords(str_replace('_',' ',$row['Field'])) . "</td>";
								echo "<td>" . strtoupper($row['Type']) . "</td>";
								echo "<td><input type='text' name='val[$i]' /></td></tr>";
								$i++;
							}
							echo "<tr><td colspan='2'><input type='submit' name='phoneSubmit1' value='Add' /></td>";
							echo "<td><input type='reset' /></td></tr>";
							echo "</table></form>";
						elseif($op == 'mod'):
							$res=mysql_query("SELECT * FROM phones WHERE phone_name='" . $name . "'");
							$row=mysql_fetch_array($res);
							$i=0;
							echo "<form action='' method='get'><table>";
							foreach($row as $key=>$val)
							{
								echo "";
								if($key !== $i)
								{
									if($key === 'model')
									{
										echo "<input type='text' name='$key' value='$val' style='display:none;'>";
										$i++;
										continue;
									}
									echo "<tr><td>" . ucwords(str_replace('_',' ',$key)) . "</td>";
									$disabled="document.getElementById('val$i').disabled";
									echo "<td><span onclick=$disabled=''>";
									echo "<input type='text' name='$key' value='$val' id='val$i' disabled='disabled' /></span></td></tr>";
									$i++;
								}
							}
							echo "<tr><td colspan='2'><input type='submit' name='phoneSubmit2' value='Update'/></tr>";
							echo "</table></form>";
						elseif($op == 'del'):
							mysql_query("DELETE FROM phones WHERE phone_name='$name'");
							if(mysql_affected_rows($con) <= 0):
								echo "Error deleting...";
							else:
								echo "Deleted";
							endif;
						endif;
					elseif($table == 'shipping'):
						if($op == 'add'):
							$res=mysql_query("SELECT * FROM orders WHERE shipping_id IS NULL AND order_date IS NOT NULL ORDER BY order_date");
							echo "<form action='' method='get'><table>";
							echo "<tr>";
							while($fld = mysql_fetch_field($res))
							{
								if($fld->name === "actual"):
									continue;
								endif;
								echo "<th class='order'>" . strtoupper(str_replace('_',' ',$fld->name)) . "</th>";
							}
							echo "<th>Expected</th>";
							echo "</tr>";
							if(!mysql_affected_rows($con)):
								echo "<tr><td colspan='9'>No pending shipments!!!</td></tr>";
							else:
								$j=0;$k=0;$t=0;
								while($row=mysql_fetch_array($res))
								{
									echo "<tr>";
									$i=0;
									foreach($row as $key=>$val)
									{
										if($key === $i):	$i++;	continue;	endif;
										if($key === "shipping_id"):
											echo "<td><div onclick=document.getElementsByClassName('id')[$j].disabled='';>";
											echo "<input type='text' name='ship_id[$j]' class='id' disabled='disabled' /></div></td>";
											$j++;
											continue;
										elseif($key === "phone_model"):
											$rr=mysql_query("SELECT phone_name FROM phones WHERE model='$val'");
											$roww=mysql_fetch_array($rr);
											$val=$roww[0];
										elseif($key === "order_id"):
											echo "<input type='text' value='$val' name='order_id[$k]' style='display:none;'/>";
											$k++;
										endif;
										echo "<td>$val</td>";
									}
									echo "<td><div onclick=document.getElementsByClassName('exp')[$t].disabled=''>";
									echo "<input type='text' name='expected[$t]' class='exp' disabled='disabled' /></div></td>";
									$t++;
									echo "</tr>";
								}
								echo "<tr><td colspan='9' style='text-align:right;padding-bottom:5px;'><input type='submit' name='shipSubmit1' value='Insert' /></td></tr>";
								echo "</table></form>";
							endif;
						elseif($op == 'mod'):
							$ship=$_POST['ship'];
							echo "<form action='' method='get'>";
							echo "<table><tr><td>Enter Actual delivery date:</td>";
							echo "<input type='text' name='ship_id' value='$ship' style='display:none;' />";
							echo "<td><input type='text' name='actual' /></td>";
							echo "<td><input type='submit' name='shipSubmit2'/></td></tr></table></form>";
						endif;
					elseif($table == 'stock'):
						$name=$_POST['name'];
						echo "<form action='' method='get'>";
						$qry = mysql_query("SELECT qty_available FROM phones WHERE phone_name = '$name'");
						$qtya = mysql_fetch_array($qry);
						$qtya = $qtya[0];
						echo "<table><tr><td colspan='3' style='text-align:center;'>Current Stock: $qtya</td></tr><tr><td>Increase in stock by:</td>";
						echo "<input type='text' name='name' value='$name' style='display:none;' />";
						echo "<td><input type='number' min='1' max='50' value='10' name='qty' /></td>";
						echo "<td><input type='submit' name='stock'/></td></tr></table></form>";
					endif;
				elseif($_GET):
					if(isset($_GET['phoneSubmit1'])):
						extract($_GET);
						$values = "";
						for($i=0;$i<count($val);$i++)
						{
							$values .= "'" . $val[$i] . "'";
							if($i < count($val) - 1)
								$values .= ", ";
						}
						$query="INSERT INTO phones VALUES ($values)";
					elseif(isset($_GET['phoneSubmit2'])):
						$values='';
						$count=count($_GET) - 3;
						foreach($_GET as $key=>$val)
						{
							if($key === 'phoneSubmit2' || $key === 'model')	continue;
							$values .= "$key = '$val'";
							if($count)
							{
								$values .= ", ";
								$count--;
							}
						}
						$query="UPDATE phones SET $values WHERE model='" . $_GET['model'] . "'";
					elseif(isset($_GET['shipSubmit1'])):
						extract($_GET);
						if(count($ship_id) === count($expected)):
							for($i=0;$i<count($ship_id);$i++)
							{
								mysql_query("UPDATE orders SET shipping_id='" . $ship_id[$i] . "' WHERE order_id='" . $order_id[$i] . "'");
								mysql_query("INSERT INTO shipping (shipping_id,expected) VALUES ('" . $ship_id[$i] . "', '" . $expected[$i] . "')");
							}
							if(mysql_affected_rows($con) <= 0):
								echo "<script>alert('Error');";
							else:
								echo "<script>alert('Inserted');";
							endif;
							//echo "location.href='update.php';</script>";
							return;
						else:
							header("location:update.php");
							return;
						endif;
					elseif(isset($_GET['shipSubmit2'])):
						extract($_GET);
						$query="UPDATE shipping SET actual = '$actual' WHERE shipping_id = '$ship_id'";
					elseif(isset($_GET['stock'])):
						extract($_GET);
						$query="UPDATE phones SET qty_available = qty_available + $qty WHERE phone_name = '$name'";
					endif;
					mysql_query($query);
					if(mysql_affected_rows($con) <= 0):
						echo "<script>alert('Error');";
					else:
						echo "<script>alert('Inserted');";
					endif;
					echo "location.href='update.php';</script>";
				endif;
			?>
		</div>
		<div class='gap2'></div>
		<div class='footer'>
			This site is a <span class='bold'>college project</span> made by <span class='bold'>Vimal Khullar</span>
		</div>
	</body>
</html>
